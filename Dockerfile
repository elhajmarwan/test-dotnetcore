FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
WORKDIR /app
COPY publish/ ./
ENTRYPOINT ["dotnet", "Web.dll"]

# # Define base image
# FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env

# # Copy project files
# WORKDIR /source
# COPY ["src/ApplicationCore/ApplicationCore.csproj", "./"]
# COPY ["src/BlazorAdmin/BlazorAdmin.csproj", "./"]
# COPY ["src/BlazorShared/BlazorShared.csproj", "./"]
# COPY ["src/Infrastructure/Infrastructure.csproj", "./"]
# COPY ["src/PublicApi/PublicApi.csproj", "./"]
# COPY ["src/Web/Web.csproj", "./"]

# # Restore
# #RUN dotnet restore 
# RUN dotnet restore "ApplicationCore.csproj"
# RUN dotnet restore "BlazorAdmin.csproj"
# RUN dotnet restore "BlazorShared.csproj"
# RUN dotnet restore "Infrastructure.csproj"
# RUN dotnet restore "PublicApi.csproj"
# RUN dotnet restore "Web.csproj"

# # Copy all source code
# COPY . .

# # Publish
# # WORKDIR /source/src
# RUN dotnet publish -c Release -o /publish

# # Runtime
# FROM mcr.microsoft.com/dotnet/aspnet:5.0
# WORKDIR /publish
# COPY --from=build-env /publish .
# ENTRYPOINT ["dotnet", "Web.dll"]
# # #ENTRYPOINT ["dotnet", "ApplicationCore.dll"]


# # # syntax=docker/dockerfile:1
# # FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
# # WORKDIR /app

# # # Copy csproj and restore as distinct layers
# # # COPY *.csproj ./
# # COPY ["src/ApplicationCore/ApplicationCore.csproj", "./"]
# # COPY ["src/BlazorAdmin/BlazorAdmin.csproj", "./BlazorAdmin"]
# # COPY ["src/BlazorShared/BlazorShared.csproj", "./BlazorShared"]
# # COPY ["src/Infrastructure/Infrastructure.csproj", "./Infrastructure"]
# # COPY ["src/PublicApi/PublicApi.csproj", "./PublicApi"]
# # COPY ["src/Web/Web.csproj", "./Web"]
# # RUN dotnet restore

# # # Copy everything else and build
# # COPY /publish ./
# # RUN dotnet publish -c Release -o out

# # # Build runtime image
# # FROM mcr.microsoft.com/dotnet/aspnet:3.1
# # WORKDIR /app
# # COPY --from=build-env /app/out .
# # ENTRYPOINT ["dotnet", "Web.dll"]
